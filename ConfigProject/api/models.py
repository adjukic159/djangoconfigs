from django.db import models

# Create your models here.

class Configs(models.Model):
    device_id = models.CharField(max_length=20)
    config_json = models.JSONField()
    active_flag = models.BooleanField(blank=True, null=True)
    origin = models.CharField(max_length=10, blank=True, null=True) # either device or web
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    class Meta:
        ordering = ['created']
