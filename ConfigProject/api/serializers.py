from rest_framework import serializers
from .models import Configs


class ConfigSerializer(serializers.Serializer):
    device_id = serializers.CharField(max_length=20)
    config_json = serializers.JSONField()  # no textfield
    active_flag = serializers.BooleanField()
    origin = serializers.CharField(max_length=10) # either device or web
    created = serializers.DateTimeField()

    def create(self, validated_data):
        """
        Create and return a new instance, given the validated data.
        """
        return Configs.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing instance, given the validated data.
        """
        instance.device_id = validated_data.get('device_id', instance.device_id)
        instance.config_json = validated_data.get('config_json', instance.config_json)
        instance.active_flag = validated_data.get('active_flag', instance.active_flag)
        instance.origin = validated_data.get('origin', instance.origin)
        instance.created = validated_data.get('created', instance.created)
        instance.save()
        return instance