from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from .models import Configs
from .serializers import ConfigSerializer

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from datetime import datetime

# Create your views here.


@api_view(['GET'])
def list_configs(request):
    """
    List all
    """
    items = Configs.objects.all()
    serializer = ConfigSerializer(items, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(['POST'])
def add_config(request, origin='', format=None):
    # data = JSONParser().parse(request)
    # serializer = ConfigSerializer(data=data)
    # if serializer.is_valid():
    data = request.data  #.dict()
    device_id = data['device_id']
    config_content = data['config_json']
    items_with_same_device_id = Configs.objects.filter(device_id=device_id)
    active_item = items_with_same_device_id.filter(active_flag=True)

    if len(active_item) > 1:
        return Response({'status': f'number of active configs {len(active_item)}'})

    elif len(active_item) == 0:

        new_item = {
            'device_id': device_id,
            'config_json': config_content,
            'active_flag': True,
            'origin': origin,
            'created': datetime.now()
        }
        new_config = ConfigSerializer(data=new_item)
        if new_config.is_valid():
            new_config.save()
            return JsonResponse(new_config.data, status=201)
        else:
            return Response(new_config.errors)

    elif len(active_item) == 1:

        active_item = active_item[0]
        active_content = active_item.config_json

        if active_content == config_content:
            return Response({'status': 'sent request same as currently active data'})

        else:
            new_item = {
                'device_id': device_id,
                'config_json': config_content,
                'active_flag': True,
                'origin': origin,
                'created': datetime.now()
            }
            new_config = ConfigSerializer(data=new_item)
            if new_config.is_valid():
                new_config.save()
                ex_active_item = ConfigSerializer(active_item, data={'active_flag': False}, partial=True)
                if ex_active_item.is_valid():
                    ex_active_item.save()
                    return JsonResponse(new_config.data, status=201)
                else:
                    return Response(ex_active_item.errors)

            else:
                return Response(new_config.errors)

    else:
        return Response({'status': 'unexpected'})


# helper post, dev effort
# @api_view(['POST'])
# # @parser_classes([JSONParser])
# def example_view(request, format=None):
#     data = request.data.dict()
#
#     device_id = data['device_id']
#     config_json = data['config_json']
#
#     return Response({'received data': device_id})
