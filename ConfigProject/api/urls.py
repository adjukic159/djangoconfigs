from django.urls import path, re_path
from .views import list_configs, add_config
urlpatterns = [
    path('api/list-configs/', list_configs),
    # path('api/web-config/', add_config),
    # path('api/device-config/', add_config),
    re_path(r'^api/(?P<origin>\w+)-config/$', add_config)
]